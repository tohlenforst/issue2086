<svelte:window on:hashchange={render} />
<svelte:component this={route}/>

<script>
    import { onMount } from 'svelte'
    export let component
    export let href = "/#"
    let route

    const render = () => {
        let url = window.location.pathname + window.location.hash
        if (url == "/") url = "/#"
        route = (href == url) ? component : null
    }

    onMount(render)
</script>
