<span bind:this={formula} on:click={copy} class:block>
    <slot />
</span>
<input bind:this={copyEl} {value} />

<script>
    import { onMount } from 'svelte'
    import katex from 'katex'
    import 'katex/dist/katex.min.css'
    export let block
    let formula
    let copyEl
    let value

    const copy = e => {
        copyEl.select()
        document.execCommand('copy')
    }

    onMount(() => {
        value = formula.innerText
        formula.innerHTML = katex.renderToString(value)
    })
</script>

<style>
    span:hover {
        color: #ff0000;
    }
    
    input {
        position: absolute;
        left: -9999px;
    }

    .block {
        display: block;
        margin: 20px;
    }
</style>
