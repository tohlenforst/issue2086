<Link href="/#" title="Home" alt="Home" />
<Link href="/#/Link1" title="Link 1" alt="Link 1" />
<Link href="/#/Link2" title="Link 2" alt="Link 2" />
<Link href="/#/Link3" title="Link 3" alt="Link 3" />
<hr />
<Route href="/#" component={Home} />
<Route href="/#/Link1" component={Link1} />
<Route href="/#/Link2" component={Link2} />
<Route href="/#/Link3" component={Link3} />

<script>
    import Link from './Link'
    import Route from './Route'

    import Home from './Links/Home'
    import Link1 from './Links/Link1'
    import Link2 from './Links/Link2'
    import Link3 from './Links/Link3'
</script>